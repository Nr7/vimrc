set langmenu=en_US
let $LANG = 'en_US'

set nocompatible
source $VIMRUNTIME/vimrc_example.vim
filetype off

" Set custom backup location
set backupdir=$HOME/.vim/vimtmp/bak//,.
set directory=$HOME/.vim/vimtmp/swp//,.
set undodir=$HOME/.vim/vimtmp/undo//,.

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'tpope/vim-fugitive'
" CtrlP
Plugin 'ctrlpvim/ctrlp.vim'
" NERDTree
Plugin 'scrooloose/nerdtree'
" NERD Commenter
Plugin 'scrooloose/nerdcommenter'
" Solarized color scheme
Plugin 'altercation/vim-colors-solarized'
" deepsea color scheme
Plugin 'hewo/vim-colorscheme-deepsea'
" ack.vim
Plugin 'mileszs/ack.vim'
" vim-airline
Plugin 'bling/vim-airline'
" YouCompleteMe
"Plugin 'Valloric/YouCompleteMe'
" Syntastic
"Plugin 'scrooloose/syntastic'
" delimitMate, automatically add closing brace/quote
Plugin 'Raimondi/delimitMate'
" Better Rainbow Parentheses
Plugin 'kien/rainbow_parentheses.vim'
" vim-clang, C&C++ autocompletion with clang
Plugin 'justmao945/vim-clang'
" AutoComplPop, automatic autocomplete popup
Plugin 'vim-scripts/AutoComplPop'
" SuperTab, Use TAB for autocompletion
Plugin 'ervandew/supertab'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let eq = ''
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      let cmd = '""' . $VIMRUNTIME . '\diff"'
      let eq = '"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
endfunction

set expandtab
set tabstop=4
set shiftwidth=4
set scrolloff=5

set encoding=utf8
set guifont=Droid_Sans_Mono_Dotted_for_Powe:h9:cANSI
let g:airline_powerline_fonts = 1
set laststatus=2

syntax enable
set background=dark
colorscheme deepsea
set number

" Open fullscreen on Windows
"au GUIEnter * simalt ~x
if has("win32")
  set lines=50 columns=165
endif

function! NumberToggle()
  if(&relativenumber == 1)
    set norelativenumber
    set number
  else
    set nonumber
    set relativenumber
  endif
endfunc

nnoremap <C-n> :call NumberToggle()<cr>

function! TabWidthToggle()
  if(&tabstop == 2)
    set tabstop=4
    set shiftwidth=4
  else
    set tabstop=2
    set shiftwidth=2
  endif
endfunc

nnoremap <C-k> :call TabWidthToggle()<cr>

" Map Shift-Tab to 'inverse' tab
inoremap <S-Tab> <C-d>

let mapleader = "\<Space>"

" NERDTree shortcut
nmap <silent> <Leader>n :NERDTreeToggle<CR>

set ignorecase  " Case insensitive searching
set smartcase   " Except when you type upper-case letters
set textwidth=0  " Disable auto-newline

" Map :W to :w
command W w

" Yank to OS clipboard
set clipboard=unnamed

" ------- CtrlP settings -------
" CtrlP filtering
let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/]\.(git|hg|svn)$',
    \ 'file': '\v\.(exe|so|dll|obj|lst|swp)$',
    \ }

" Set a custom ctrlp root market
let g:ctrlp_root_markers = ['.ignore']

"Use Ag for CtrlP
"let g:ctrlp_user_command = 'ag -l --nocolor -g "" %s'

" ------- Syntastic settings -------
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*

"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0

" ------- NERDCommenter settings -------------
let g:NERDSpaceDelims = 1 " 1 space between the comment delim & comment
let g:NERDAltDelims_c = 1 " Use // for comments on c

" ------- Rainbow Parentheses settings -------
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" ------- SuperTab settings ------------------
" Swap cycle direction
let g:SuperTabDefaultCompletionType = '<c-n>'

" ------- delimitMate settings ----------------
let delimitMate_expand_cr = 1

" automatically open and close the popup menu / preview window      
"au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif      
"set completeopt=menuone,menu,longest,preview
"autocmd CompleteDone * pclose
let g:SuperTabClosePreviewOnPopupClose = 1

" --------- vim-clang settings ---------------
"let g:clang_c_options = '-std=c99 -w -arc=arm -D __ICCARM__=1'

" ----------- Ack.vim settings ---------------
"Use ag instead of Ack
let g:ackprg = 'ag --smart-case'
"Don't jump to the first result immediately
map <C-F> :Ack! 

" Start from home directory by default
cd ~
" Set current directory to opened file otherwise
au VimEnter * silent! cd %:h

" Allow project specific vimrc files
"set exrc
" Disallow unsafe commands in project specific vimrc files
"set secure
